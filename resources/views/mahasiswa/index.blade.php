@extends('layout/main')

@section('title', 'Mahasiswa')


@section('container')
  <div class="container">
   <div class="row">
      <div class="column">
    <h1 class="mt-3">Daftar Mahasiswa</h1>
    <table class="table">
    	<thead class="table-dark">
    		<th scope="col">#</th>
    		<th scope="col">Nama</th>
    		<th scope="col">NIM</th>
    		<th scope="col">Email</th>
    		<th scope="col">Jurusan</th>
    		<th scope="col">Aksi</th>
    	</thead>
    	<tbody>
    		<th scope="row">1</th>
    		<td>Auriel Edo</td>
    		<td>205150709111011</td>
    		<td>aurieledo@gmail.com</td>
    		<td>Teknologi Informasi</td>
    		<td>
    			<a href="" class="btn btn-success">edit</a>
    			<a href="" class="btn btn-danger">delete</a>
    		</td>
    	</tbody>
    </table>
      </div>
    </div>
  </div>
@endsection